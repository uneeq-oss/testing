## Session Test - Cypress E2E Testing
This directory contains a framework and structure for writing
Cypress e2e tests. Cypress test files can be added to the `tests`
directory.

### Description
**This set of cypress tests will validate:**
- Call to action is displayed and clicked to start session
- That a session can start in all layout modes
- User can ask a text question
- Session can be ended
- Language strings can be used to display alternative UI texts (in other languages)

**This set of cypress tests will NOT validate:**
- That the video stream contains a digital human
- The digital human can speak
- The user can speak to the digital human

### Installation
`npm install`

### Running Tests (locally)
`npm test`

### Environment Variables
- Provide your cypress-id in `cypress.json` to replace value `[cypress-project-id]`
- Provide the URL to the application to test in `cypress.json` to replace value `[url-to-test]`
- To build a docker image, you will need to update your cypress key to replace value `[cypress-key]`

### Docker
A Docker file has been configured here that will build and run all tests.

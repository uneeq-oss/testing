Cypress.Commands.add('clickCTA', () => {
    cy.get('iframe')
    cy.frameLoaded('iframe')
    cy.iframe().find('[data-test="cta-container"]')
    cy.wait(1000)
    cy.iframe().find('[data-test="cta-container"] .cta-thumbnail-box').click()
})

Cypress.Commands.add('getDigitalHumanVideo', () => {
    cy.wait(12000)
    // Test will wait for 4 seconds for video element to render + 8 seconds (above) = 12 Second max
    cy.iframe().find('.avatarVideoContainer video')
})

Cypress.Commands.add('getUserInputElement', () => {
    cy.iframe().find('app-user-input-overlay');
})

Cypress.Commands.add('startAndStopRecording', () => {
    cy.iframe().find('#recordStartBtn').click();
    cy.iframe().find('#recordStopBtn').click();
})

Cypress.Commands.add('askQuestionAndRenderContent', () => {
    cy.get('#testQuestion1').click()
    cy.iframe().find('#content')

    // Should Render content coming from NLP
    cy.iframe().find('[data-test="content"] .container.open').contains('Example Content')
    // Rendered content should allow inline styles
    cy.iframe().find('[data-test="content"] .container.open h1').should('have.css', 'color', 'rgb(255, 165, 0)')
    // Rendered content should contain custom styles
    cy.iframe().find('[data-test="content"] .container.open h1').should('have.css', 'font-family', `Nunito, sans-serif`)
})

export function basicHostedExperienceTestRun() {
    it('can click the cta', () => {
        cy.clickCTA()
    })
    it('can get DH video', () => {
        cy.getDigitalHumanVideo()
    })
    it('displays the user input element', () => {
        cy.window().then((win) => {
            win.uneeqSetShowUserInputInterface(true)
        });
        cy.getUserInputElement()
    })
    it('can perform a recording', () => {
        cy.startAndStopRecording()
    })
    it('can ask a question and render content', () => {
        cy.askQuestionAndRenderContent()
    })
    it('can hide content', () => {
        cy.iframe().find('[data-test="hide-content"]').click();
    })
}

export function endSessionTest() {

    it( 'should end the session from the parent frame', () => {
        cy.window().then((win) => {
            win.uneeqEndSession()
        })
    })

}
import 'cypress-iframe'
import {basicHostedExperienceTestRun, endSessionTest} from "../cypress/support/command";

describe('Hosted Experience Full Screen', () => {

    it('loads hosted experience page', () => {
        cy.visit(Cypress.env('interactions_demo_url'))
        cy.window().then((win) => {
            win.window.uneeqInteractionsOptions = {
                personaShareId: win.document.getElementById('personaShareId').value,
                layoutMode: 'fullScreen',
                displayCallToAction: true,
                customStyles: `h1 {
                    font-family: 'Nunito', sans-serif;
                }`
            }
            win.addHostedExperienceScript(false);
        })
        cy.viewport(1250, 900)
    })

    basicHostedExperienceTestRun()

    it( 'digital human video should fill the screen', () => {
        cy.viewport(1250, 900)
        cy.document().then( doc => {
            cy.iframe().find('app-avatar-video video').invoke('innerWidth').should('be.approximately', doc.body.clientWidth, 1)
            cy.iframe().find('app-avatar-video video').invoke('innerHeight').should('be.approximately', doc.body.clientHeight, 1)
        });
    })

    endSessionTest()

});

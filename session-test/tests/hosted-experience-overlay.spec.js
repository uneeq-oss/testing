import 'cypress-iframe'
import {basicHostedExperienceTestRun, endSessionTest} from "../cypress/support/command";

const customCallToActionText = 'Custom default call to action text'
const customRecordingSpeakNowPrompt = '平片漢Ж大Æ👋'
const baseTextInputPlaceholder = 'Type here...'

// Tests that are not specific to a layout mode are run in this test file

describe('Hosted Experience Overlay', () => {

    it('loads hosted experience page', () => {
        cy.viewport(1250, 900)
        cy.visit(Cypress.env('interactions_demo_url'))

        cy.window().then((win) => {
            win.window.uneeqInteractionsOptions = {
                personaShareId: win.document.getElementById('personaShareId').value,
                layoutMode: 'overlay',
                displayCallToAction: true,
                customStyles: `h1 {
                    font-family: 'Nunito', sans-serif;
                }`,
                languageStrings: {
                    'en-US': {
                        callToActionText: customCallToActionText
                    },
                    default: {
                        recordingSpeakNowPrompt: customRecordingSpeakNowPrompt
                    }
                }
            }
            win.addHostedExperienceScript(false)
        })
    })

    basicHostedExperienceTestRun()

    it( 'Digital human video should be overlay size', () => {
        cy.viewport(1250, 900)
        cy.iframe().find('app-avatar-video video').invoke('innerWidth').should('be.equal', 310 )
        cy.iframe().find('app-avatar-video video').invoke('innerHeight').should('be.equal', 190)
    })

    it( 'Should get a CustomData message with data', () => {
        // The NLP returns this specific custom data
        const expectedCustomData = '{"exampleData1":{"exampleValue":true},"exampleData2":"0123456789"}'
        cy.askQuestionAndRenderContent()
        // Hosted experience demo app will load any custom data into a hidden textarea which we read from here
        cy.get('#customData').contains(expectedCustomData)
    })

    it( 'Language Strings: Should render custom en-US in call to action', () => {
        cy.iframe().find('.pause-dh-btn').click({force: true})
        cy.iframe().find('#cta-text .cta-message').contains(customCallToActionText)
        cy.iframe().find('#cta-text').click()
    })

    it( 'Language Strings: Should render non latin characters as "default" language for ' +
        'custom recording "speak now" prompt', () => {
        cy.iframe().find('#recordStartBtn').click()
        cy.iframe().find('.input-container.recording-live .label').contains(customRecordingSpeakNowPrompt)
        cy.iframe().find('#recordStopBtn').click()
    })

    it( 'Language Strings: Should render base text when no custom language string is provided', () => {
        cy.iframe().find('#questionInput').invoke('attr', 'placeholder')
            .should('eq', baseTextInputPlaceholder)
    })

    endSessionTest()

});

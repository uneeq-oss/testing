import 'cypress-iframe'
import {endSessionTest} from "../cypress/support/command";

// This test is specifically focused on the script injection to ensure it works as customers would use it:
// setting window.uneeqInteractionsOptions
// loading the script in the <head></head> tag (not dynamically injecting it as the other tests do).

describe('Hosted Experience Script Injection', () => {

    it('loads hosted experience page', () => {
        cy.visit(Cypress.env('interactions_demo_url') + '?autoinit=true')
    })
    it('can click the cta', () => {
        cy.clickCTA();
    })
    it( 'can get DH video', () => {
        cy.getDigitalHumanVideo();
    })

    endSessionTest()

})
